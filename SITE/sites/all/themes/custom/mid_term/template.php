<?php

function midterm_preprocess(&$variables)
{
	$page = $variables['page'];
	$content_class = "";
	
	if (empty($page['left_column']))
	{
		$content_class = "no-left-padding";
	}
	
	
	$variables['midterm']['content_class'] = $content_class;
}

?>