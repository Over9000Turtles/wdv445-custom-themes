<?php

/**
 * Gamit theme stuff
 */
 
// if the right column region is populated, add a class - 'right-callout' - to the classes array

function theme_preprocess_html(&$variables){
    if(!empty($variables['page']['right_callout'])){
        $variables['classes_array'][] = "right-callout";
    }    
} 

function theme_preprocess_page(&$variables){

    $widthClasses = array();
    $widthClasses['main'] = "";
    $widthClasses['column'] = "";
        
    
    // if either column is populated    
    if (!empty($variables['page']['right_callout'])){
        $widthClasses['main'] = "three-fourths";
        $widthClasses['column'] = "one-fourth";
    // if no columns are populated    
    } else {
        $widthClasses['main'] = "";
    }
    
    $variables['theme_widthClasses'] = $widthClasses;
}

 ?>
 
 
 
 
 
 
 
 
 
 
 
 