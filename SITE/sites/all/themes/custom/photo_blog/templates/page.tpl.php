   
    <div class="site-container">
        
        <div class="gradient-overlay">
        </div>
        
        <!-- header -->
        <div class="header-wrapper container">
            <div class="site-title">
                <p>Photography Blog</p>
            </div>
            
            <div class="menu-wrapper clearfix">
               <?php print theme('links',array('links'=>$main_menu));?>    
            </div>
        </div>
        
        <!-- content -->
        <div class="content-container container">
            
            <div class="featured-photo container">
               <!-- <img src="img/the_birds.jpg" />
                <div class="label">Photo: The birds</div>-->
                <?php print render($page['feature_image']);?>
            </div>
            
            <!-- tabs will go here -->
            <div class="tab-container container">
            </div>
            
            <div class="title inner-container">
                <h1><?php print $title; ?></h1>
            </div>
            
            <div class="content inner-container clearfix shadow background">
                
                <?php if ( !empty($page['left_column'])) { ?>
                
                <div class="left-column column region one-fourth left">
                    <!--<div class="region-inner">
                        <ul class="photo-categories">
                            <li>
                                <div class="block">
                                    <div class="inner">
                                        <img src="img/boz_skate.jpg" />
                                        <div class="category">
                                            <p class="label">
                                                <a href="#" class="more">Friends</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="block">
                                    <div class="inner">
                                        <img src="img/ferrari.jpg" />
                                        <div class="category">
                                            <p class="label">
                                                <a href="#" class="more">Historic Races</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="block">
                                    <div class="inner">
                                        <img src="img/miner_show.jpg" />
                                        <div class="category">
                                            <p class="label">
                                                <a href="#" class="more">Music</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>-->
                    
                    <?php print render($page['left_column']);?>
                </div>
                
                <?php } ?>
                
                <div class="main-content three-fourths left">
                    <div class="content">
                    
                    <?php print $variables['photo_blog']['content_class']; ?>
                        
                        <!-- messages will go here -->
                        <div id="messages">
                        <?php print $messages; ?>
                        </div>
                        <?php print render($page['content']);?>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- footer -->
        <div class="footer-container container">
            <div class="footer-content inner-container">
                <p><?php print render($page['footer']); ?></p>
            </div>
        </div>
        
    </div>
</body>

</html>
